#!/bin/bash
INPUT='\033[1;36m'
OUTPUT='\033[1;33m'
EXPECTED='\033[0;32m'
DEFAULT='\033[0m'
echo -n "./main input: '$INPUT$1$DEFAULT', output: '$OUTPUT" ; echo -n $1 | ./main ; echo -n "$DEFAULT'" ; echo ", expected: '$EXPECTED$2$DEFAULT'"
