.phony: start clean test main

COMP = nasm -g -f elf64
SRC = src
OUT = out

ASM = $(wildcard $(SRC)/*.asm)
OBJ = $(patsubst %.asm, %.o, $(ASM))

start: main
	@echo Done!

main: $(OBJ)
	ld -o $@ $^

%.o: %.asm
	@echo -n $@
	@echo " " $<
	$(COMP) -o $@ $< -Isrc

clean:
	rm -f $(SRC)/*.o
	rm -f main

test:
	@echo -e '${GREEN}-=-=-=-=-=-Tests-=-=-=-=-=-${DEF}'
	@sh test.sh hello Hello,\ ;
	@sh test.sh world World\ ;
	@sh test.sh and and\ ;
	@sh test.sh assembly Assembly!
	@echo -en 'Full input: '
	@${TST_INP}
	@echo "\n"
	@sh test.sh goodbye "Key ne byl naiden";
	@sh test.sh '' "Key ne byl naiden";
	@echo 
	@sh test.sh $(TST_INP_BIG) "Failed to parse key from console";
	@echo -e '${GREEN}-=-=-=-=-=-=End=-=-=-=-=-=-${DEF}'


TST_INP = echo -n 'hello' | ./main;echo -n 'world' | ./main;echo -n 'and' | ./main;echo -n 'assembly' | ./main;
TST_INP_BIG = 256CharStringooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
GREEN = \033[0;32m
DEF = \033[0m
