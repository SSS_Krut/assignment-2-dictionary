;dict.asm
%include "lib.inc"
section .text

global find_word

; pnext |  key  | value
;ptrnext|shrtstr|longstr
;   8   |  inf  |  inf

find_word: 
    ;rdi - ptr_key
    ;rsi - ptr_list
    push rbx
    push rbp
    mov rbx, rdi;key
    mov rbp, rsi;list
    .start_element:
        mov rdi, rbx
        lea rsi, [rbp+8]
        call string_equals
        
        test rax, rax
        jnz .found_word

        mov rbp, [rbp] ; set pointer to next element
        test rbp, rbp
        jne .start_element
    .end_of_list:
        xor rax, rax
        jmp .end
    .found_word:
        lea rax, [rbp+8]
    .end:
        pop rbp
        pop rbx
        ret

