%define ptr_next 0

%macro colon 2
    %2:
        dq ptr_next
        db %1,0
    %define ptr_next %2
%endmacro
