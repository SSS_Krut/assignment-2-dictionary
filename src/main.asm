;main.asm
%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFF_SIZE 256

section .bss
cin: resb BUFF_SIZE
section .rodata
err_failed_parsing: db "Failed to parse key from console",0
err_failed_found: db "Key ne byl naiden",0
section .text
global _start
_start:
    mov rdi, cin
    mov rsi, BUFF_SIZE
    call read_string
    test rax, rax
    je .err_failed_parsing
    mov rdi, cin
    mov rsi, wrd_hello
    call find_word
    test rax, rax
    je .err_failed_found
    push rax
    mov rdi, rax
    call string_length
    pop r8
    add r8, rax
    add r8, 1
    mov rdi, r8
    call print_string
    jmp .end
    .err_failed_parsing:
        mov rdi, err_failed_parsing
        jmp .err
    .err_failed_found:
        mov rdi, err_failed_found   
    .err:
        call print_err_string
    .end:
        xor rdi, rdi
        jmp exit
